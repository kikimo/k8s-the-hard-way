#!/bin/sh

BASE_dir=$(dirname $(dirname $(realpath $0)))
BIN=$BASE_dir/bin
SCRIPT_DIR=$BASE_dir/scripts
CFG_DIR=$BASE_dir/configs
CERT_DIR=$BASE_dir/certs

$BIN/kubectl get nodes --kubeconfig $CFG_DIR/admin.kubeconfig
