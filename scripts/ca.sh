#!/bin/sh

BASE_dir=$(dirname $(dirname $(realpath $0)))
BIN=$BASE_dir/bin
SCRIPT_DIR=$BASE_dir/scripts
CERT_DIR=$BASE_dir/certs
rm -rf $CERT_DIR && mkdir $CERT_DIR && cd $CERT_DIR

# 1. Certificate Authority
cat > $CERT_DIR/ca-config.json <<EOF
{
  "signing": {
    "default": {
      "expiry": "8760h"
    },
    "profiles": {
      "kubernetes": {
        "usages": ["signing", "key encipherment", "server auth", "client auth"],
        "expiry": "8760h"
      }
    }
  }
}
EOF

cat > $CERT_DIR/ca-csr.json <<EOF
{
  "CN": "Kubernetes",
  "key": {
    "algo": "rsa",
    "size": 2048
  },
  "names": [
    {
      "C": "US",
      "L": "Portland",
      "O": "Kubernetes",
      "OU": "CA",
      "ST": "Oregon"
    }
  ]
}
EOF

$BIN/cfssl gencert -initca $CERT_DIR/ca-csr.json | $BIN/cfssljson -bare ca

# 2. The Admin Client Certificate
cat > admin-csr.json <<EOF
{
  "CN": "admin",
  "key": {
    "algo": "rsa",
    "size": 2048
  },
  "names": [
    {
      "C": "US",
      "L": "Portland",
      "O": "system:masters",
      "OU": "Kubernetes The Hard Way",
      "ST": "Oregon"
    }
  ]
}
EOF

$BIN/cfssl gencert \
  -ca=$CERT_DIR/ca.pem \
  -ca-key=$CERT_DIR/ca-key.pem \
  -config=$CERT_DIR/ca-config.json \
  -profile=kubernetes \
  $CERT_DIR/admin-csr.json | $BIN/cfssljson -bare admin

# 3. The Kubelet Client Certificates
$SCRIPT_DIR/ca-worker.sh worker-0 192.168.56.101
$SCRIPT_DIR/ca-worker.sh worker-1 192.168.56.102
$SCRIPT_DIR/ca-worker.sh worker-2 192.168.56.103

# 4. The Controller Manager Client Certificate
cat > $CERT_DIR/kube-controller-manager-csr.json <<EOF
{
  "CN": "system:kube-controller-manager",
  "key": {
    "algo": "rsa",
    "size": 2048
  },
  "names": [
    {
      "C": "US",
      "L": "Portland",
      "O": "system:kube-controller-manager",
      "OU": "Kubernetes The Hard Way",
      "ST": "Oregon"
    }
  ]
}
EOF

$BIN/cfssl gencert \
  -ca=$CERT_DIR/ca.pem \
  -ca-key=$CERT_DIR/ca-key.pem \
  -config=$CERT_DIR/ca-config.json \
  -profile=kubernetes \
  $CERT_DIR/kube-controller-manager-csr.json | $BIN/cfssljson -bare kube-controller-manager
 
# 5. The Kube Proxy Client Certificate
cat > $CERT_DIR/kube-proxy-csr.json <<EOF
{
  "CN": "system:kube-proxy",
  "key": {
    "algo": "rsa",
    "size": 2048
  },
  "names": [
    {
      "C": "US",
      "L": "Portland",
      "O": "system:node-proxier",
      "OU": "Kubernetes The Hard Way",
      "ST": "Oregon"
    }
  ]
}
EOF

$BIN/cfssl gencert \
  -ca=$CERT_DIR/ca.pem \
  -ca-key=$CERT_DIR/ca-key.pem \
  -config=$CERT_DIR/ca-config.json \
  -profile=kubernetes \
  $CERT_DIR/kube-proxy-csr.json | $BIN/cfssljson -bare kube-proxy

# 6.The Scheduler Client Certificate
cat > $CERT_DIR/kube-scheduler-csr.json <<EOF
{
  "CN": "system:kube-scheduler",
  "key": {
    "algo": "rsa",
    "size": 2048
  },
  "names": [
    {
      "C": "US",
      "L": "Portland",
      "O": "system:kube-scheduler",
      "OU": "Kubernetes The Hard Way",
      "ST": "Oregon"
    }
  ]
}
EOF

$BIN/cfssl gencert \
  -ca=$CERT_DIR/ca.pem \
  -ca-key=$CERT_DIR/ca-key.pem \
  -config=$CERT_DIR/ca-config.json \
  -profile=kubernetes \
  $CERT_DIR/kube-scheduler-csr.json | $BIN/cfssljson -bare kube-scheduler

# 7.The Kubernetes API Server Certificate
cat > $CERT_DIR/kubernetes-csr.json <<EOF
{
  "CN": "kubernetes",
  "key": {
    "algo": "rsa",
    "size": 2048
  },
  "names": [
    {
      "C": "US",
      "L": "Portland",
      "O": "Kubernetes",
      "OU": "Kubernetes The Hard Way",
      "ST": "Oregon"
    }
  ]
}
EOF

$BIN/cfssl gencert \
  -ca=$CERT_DIR/ca.pem \
  -ca-key=$CERT_DIR/ca-key.pem \
  -config=$CERT_DIR/ca-config.json \
  -hostname=10.32.0.1,192.168.56.101,192.168.56.102,192.168.56.103,worker-0,worker-1,worker-2,127.0.0.1 \
  -profile=kubernetes \
  $CERT_DIR/kubernetes-csr.json | $BIN/cfssljson -bare kubernetes

# 8.The Service Account Key Pair
cat > $CERT_DIR/service-account-csr.json <<EOF
{
  "CN": "service-accounts",
  "key": {
    "algo": "rsa",
    "size": 2048
  },
  "names": [
    {
      "C": "US",
      "L": "Portland",
      "O": "Kubernetes",
      "OU": "Kubernetes The Hard Way",
      "ST": "Oregon"
    }
  ]
}
EOF

$BIN/cfssl gencert \
  -ca=$CERT_DIR/ca.pem \
  -ca-key=$CERT_DIR/ca-key.pem \
  -config=$CERT_DIR/ca-config.json \
  -profile=kubernetes \
  $CERT_DIR/service-account-csr.json | $BIN/cfssljson -bare service-account

