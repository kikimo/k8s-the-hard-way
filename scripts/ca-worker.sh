#!/bin/sh

BASE_dir=$(dirname $(dirname $(realpath $0)))
BIN=$BASE_dir/bin
CERT_DIR=$BASE_dir/certs

HOSTNAME=$1
HOSTIP=$2

cat > $CERT_DIR/${HOSTNAME}-csr.json <<EOF
{
  "CN": "system:node:${HOSTNAME}",
  "key": {
    "algo": "rsa",
    "size": 2048
  },
  "names": [
    {
      "C": "US",
      "L": "Portland",
      "O": "system:nodes",
      "OU": "Kubernetes The Hard Way",
      "ST": "Oregon"
    }
  ]
}
EOF

$BIN/cfssl gencert \
  -ca=$CERT_DIR/ca.pem \
  -ca-key=$CERT_DIR/ca-key.pem \
  -config=$CERT_DIR/ca-config.json \
  -hostname=${HOSTNAME},${HOSTIP} \
  -profile=kubernetes \
  $CERT_DIR/${HOSTNAME}-csr.json | $BIN/cfssljson -bare ${HOSTNAME}
