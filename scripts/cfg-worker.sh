#!/bin/sh

BASE_dir=$(dirname $(dirname $(realpath $0)))
BIN=$BASE_dir/bin
SCRIPT_DIR=$BASE_dir/scripts
CERT_DIR=$BASE_dir/certs
CFG_DIR=$BASE_dir/configs

HOSTNAME=$1
HOSTIP=$2

$BIN/kubectl config set-cluster local \
  --certificate-authority=$CERT_DIR/ca.pem \
  --embed-certs=true \
  --server=https://${HOSTIP}:6443 \
  --kubeconfig=$CFG_DIR/${HOSTNAME}.kubeconfig

$BIN/kubectl config set-credentials system:node:${HOSTNAME} \
  --client-certificate=$CERT_DIR/${HOSTNAME}.pem \
  --client-key=$CERT_DIR/${HOSTNAME}-key.pem \
  --embed-certs=true \
  --kubeconfig=$CFG_DIR/${HOSTNAME}.kubeconfig

$BIN/kubectl config set-context default \
  --cluster=local \
  --user=system:node:${HOSTNAME} \
  --kubeconfig=$CFG_DIR/${HOSTNAME}.kubeconfig

$BIN/kubectl config use-context default --kubeconfig=$CFG_DIR/${HOSTNAME}.kubeconfig
