#!/bin/sh

BASE_dir=$(dirname $(dirname $(realpath $0)))
BIN=$BASE_dir/bin
SCRIPT_DIR=$BASE_dir/scripts
CFG_DIR=$BASE_dir/configs
CERT_DIR=$BASE_dir/certs

$BIN/kubectl get componentstatuses --kubeconfig $CFG_DIR/admin.kubeconfig && echo
curl --cacert $CERT_DIR/ca.pem https://127.0.0.1:6443/version && echo
