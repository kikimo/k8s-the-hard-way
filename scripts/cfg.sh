#!/bin/sh

BASE_dir=$(dirname $(dirname $(realpath $0)))
BIN=$BASE_dir/bin
SCRIPT_DIR=$BASE_dir/scripts
CFG_DIR=$BASE_dir/configs
CERT_DIR=$BASE_dir/certs
rm -rf $CFG_DIR && mkdir $CFG_DIR && cd $CFG_DIR

CLUSTER=local
KUBERNETES_PUBLIC_ADDRESS=192.168.56.101

# 1. The kubelet Kubernetes Configuration File
$SCRIPT_DIR/cfg-worker.sh worker-0 $KUBERNETES_PUBLIC_ADDRESS
$SCRIPT_DIR/cfg-worker.sh worker-1 $KUBERNETES_PUBLIC_ADDRESS
$SCRIPT_DIR/cfg-worker.sh worker-2 $KUBERNETES_PUBLIC_ADDRESS

# 2. The kube-proxy Kubernetes Configuration File
$BIN/kubectl config set-cluster $CLUSTER \
  --certificate-authority=$CERT_DIR/ca.pem \
  --embed-certs=true \
  --server=https://${KUBERNETES_PUBLIC_ADDRESS}:6443 \
  --kubeconfig=kube-proxy.kubeconfig

$BIN/kubectl config set-credentials system:kube-proxy \
  --client-certificate=$CERT_DIR/kube-proxy.pem \
  --client-key=$CERT_DIR/kube-proxy-key.pem \
  --embed-certs=true \
  --kubeconfig=$CFG_DIR/kube-proxy.kubeconfig

$BIN/kubectl config set-context default \
  --cluster=$CLUSTER \
  --user=system:kube-proxy \
  --kubeconfig=$CFG_DIR/kube-proxy.kubeconfig

$BIN/kubectl config use-context default --kubeconfig=kube-proxy.kubeconfig

# 3. The kube-controller-manager Kubernetes Configuration File
$BIN/kubectl config set-cluster $CLUSTER \
  --certificate-authority=$CERT_DIR/ca.pem \
  --embed-certs=true \
  --server=https://127.0.0.1:6443 \
  --kubeconfig=$CFG_DIR/kube-controller-manager.kubeconfig

$BIN/kubectl config set-credentials system:kube-controller-manager \
  --client-certificate=$CERT_DIR/kube-controller-manager.pem \
  --client-key=$CERT_DIR/kube-controller-manager-key.pem \
  --embed-certs=true \
  --kubeconfig=$CFG_DIR/kube-controller-manager.kubeconfig

$BIN/kubectl config set-context default \
  --cluster=$CLUSTER \
  --user=system:kube-controller-manager \
  --kubeconfig=$CFG_DIR/kube-controller-manager.kubeconfig

$BIN/kubectl config use-context default --kubeconfig=$CFG_DIR/kube-controller-manager.kubeconfig

# 4. The kube-scheduler Kubernetes Configuration File
kubectl config set-cluster $CLUSTER \
  --certificate-authority=$CERT_DIR/ca.pem \
  --embed-certs=true \
  --server=https://127.0.0.1:6443 \
  --kubeconfig=$CFG_DIR/kube-scheduler.kubeconfig

kubectl config set-credentials system:kube-scheduler \
  --client-certificate=$CERT_DIR/kube-scheduler.pem \
  --client-key=$CERT_DIR/kube-scheduler-key.pem \
  --embed-certs=true \
  --kubeconfig=$CFG_DIR/kube-scheduler.kubeconfig

kubectl config set-context default \
  --cluster=$CLUSTER \
  --user=system:kube-scheduler \
  --kubeconfig=$CFG_DIR/kube-scheduler.kubeconfig

kubectl config use-context default --kubeconfig=$CFG_DIR/kube-scheduler.kubeconfig

# 5. The admin Kubernetes Configuration File
kubectl config set-cluster $CLUSTER \
  --certificate-authority=$CERT_DIR/ca.pem \
  --embed-certs=true \
  --server=https://127.0.0.1:6443 \
  --kubeconfig=$CFG_DIR/admin.kubeconfig

kubectl config set-credentials admin \
  --client-certificate=$CERT_DIR/admin.pem \
  --client-key=$CERT_DIR/admin-key.pem \
  --embed-certs=true \
  --kubeconfig=$CFG_DIR/admin.kubeconfig

kubectl config set-context default \
  --cluster=$CLUSTER \
  --user=admin \
  --kubeconfig=$CFG_DIR/admin.kubeconfig

kubectl config use-context default --kubeconfig=$CFG_DIR/admin.kubeconfig

# 6. Generating the Data Encryption Config and Key
ENCRYPTION_KEY=$(head -c 32 /dev/urandom | base64)
cat > $CFG_DIR/encryption-config.yaml <<EOF
kind: EncryptionConfig
apiVersion: v1
resources:
  - resources:
      - secrets
    providers:
      - aescbc:
          keys:
            - name: key1
              secret: ${ENCRYPTION_KEY}
      - identity: {}
EOF
