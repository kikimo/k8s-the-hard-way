#!/bin/sh

BASE_dir=$(dirname $(dirname $(realpath $0)))
BIN=$BASE_dir/bin
SCRIPT_DIR=$BASE_dir/scripts
CERT_DIR=$BASE_dir/certs

HOSTNAME=$(hostname)
HOSTIP=$(ip addr | grep 192 | awk '{print $2}' | cut -d '/' -f 1)

sudo systemctl stop etcd

rm -rf /etc/etcd /var/lib/etcd
mkdir -p /etc/etcd /var/lib/etcd
cp $CERT_DIR/ca.pem $CERT_DIR/kubernetes-key.pem $CERT_DIR/kubernetes.pem /etc/etcd/
cp $BIN/etcd* /usr/local/bin/

cat <<EOF | sudo tee /etc/systemd/system/etcd.service
[Unit]
Description=etcd
Documentation=https://github.com/coreos

[Service]
Type=notify
ExecStart=/usr/local/bin/etcd \\
  --name ${HOSTNAME} \\
  --cert-file=/etc/etcd/kubernetes.pem \\
  --key-file=/etc/etcd/kubernetes-key.pem \\
  --peer-cert-file=/etc/etcd/kubernetes.pem \\
  --peer-key-file=/etc/etcd/kubernetes-key.pem \\
  --trusted-ca-file=/etc/etcd/ca.pem \\
  --peer-trusted-ca-file=/etc/etcd/ca.pem \\
  --peer-client-cert-auth \\
  --client-cert-auth \\
  --initial-advertise-peer-urls https://${HOSTIP}:2380 \\
  --listen-peer-urls https://${HOSTIP}:2380 \\
  --listen-client-urls https://${HOSTIP}:2379,https://127.0.0.1:2379 \\
  --advertise-client-urls https://${HOSTIP}:2379 \\
  --initial-cluster-token etcd-cluster-0 \\
  --initial-cluster worker-0=https://192.168.56.101:2380,worker-1=https://192.168.56.102:2380,worker-2=https://192.168.56.103:2380 \\
  --initial-cluster-state new \\
  --data-dir=/var/lib/etcd
Restart=on-failure
RestartSec=5

[Install]
WantedBy=multi-user.target
EOF

sudo systemctl daemon-reload
sudo systemctl enable etcd
sudo systemctl start etcd
