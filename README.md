# k8s-the-hard-way

scripts for [Kubernetes The Hard Way](https://github.com/kelseyhightower/kubernetes-the-hard-way).

## 1. getting started

1. generate ca certs and config files

```shell
# generate all ca certs
$ ./scripts/ca.sh

# generate all k8s configs
$ ./scripts/cfg.sh
```

2. install etcd

```shell
# init && start && check etcd
$ ./scripts/etcd.sh
$ ./scripts/check-etec.sh
```

3. install master

```shell
$ ./scripts/master.sh
$ ./scripts/check-master.sh
```

4. install node

TODO
